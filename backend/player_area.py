#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.gridlayout import GridLayout
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty
)
from kivy.uix.widget import Widget
from kivy.clock import Clock, mainthread


class PlayerArea(GridLayout):
    '''
    Part of game screen belonging to a player.
    '''
    points_container = ObjectProperty()
    timer = ObjectProperty()

    match_started = False
    score = NumericProperty(0)
    player_name = StringProperty('unknown')

    def __init__(self, **kwargs):
        super(PlayerArea, self).__init__(**kwargs)
        self._register_events()
        self._bind_events()

    def _register_events(self):
        self.register_event_type('on_timeout')

    def on_timeout(self):
        pass

    @mainthread
    def _bind_events(self):
        self.game_board.bind(
            on_match_start=lambda x: self._on_match_start()
        )
        self.timer.bind(
            on_timeout=lambda x: self._on_timeout()
        )

    def _on_match_start(self):
        self.match_started = True

    def _on_timeout(self):
        self.dispatch('on_timeout')

    def start_timer(self):
        if self.match_started:
            self.timer.start_timer()

    def stop_timer(self):
        self.timer.stop_timer()

    def reset(self, player_name):
        self.player_name = player_name
        self.match_started = False
        self.score = 0
        self.points_container.reset()
        self.stop_timer()


class Timer(Widget):

    start_angle = NumericProperty(0)
    game_board = ObjectProperty()
    player = ObjectProperty()
    started = BooleanProperty(False)

    _timer_freq = 0.05
    turn_duration = 10

    def __init__(self, **kwargs):
        super(Timer, self).__init__(**kwargs)
        self.register_event_type('on_timeout')

    def on_timeout(self):
        pass

    def start_timer(self):
        self.start_angle = 0
        if not self.started:
            self.started = True
            Clock.schedule_interval(
                self._decay_angle,
                self._timer_freq
            )

    def _decay_angle(self, dt):
        self._compute_angle(dt, self.turn_duration)
        if self.start_angle >= 360:
            self.stop_timer()
            self.dispatch('on_timeout')

    def _compute_angle(self, dt, seconds):
        delay = dt - self._timer_freq
        angle = (360 * (self._timer_freq + delay)) / seconds
        self.start_angle += angle

    def stop_timer(self):
        Clock.unschedule(self._decay_angle)
        self.started = False
        self.start_angle = 0
