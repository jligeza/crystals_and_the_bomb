#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.graphics.texture import Texture
from kivy.uix.gridlayout import GridLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.lang import Builder


Builder.load_string('''
<GradientButton>:
    cols: 1

    gradient: [(.3,.1,.1,1),(.9,.1,.1,1)]

    canvas.before:
        RoundedRectangle:
            size: self.size
            pos: self.pos
            radius: [dp(10)]
            texture: self.vertical_gradient(*self.gradient)
''')


class GradientButton(ButtonBehavior, GridLayout):

    @staticmethod
    def horizontal_gradient(rgba_left, rgba_right):
        texture = Texture.create(size=(2, 1), colorfmt="rgba")
        pixels = rgba_left + rgba_right
        pixels = [chr(int(v * 255)) for v in pixels]
        buf = ''.join(pixels)
        texture.blit_buffer(buf, colorfmt='rgba', bufferfmt='ubyte')
        return texture

    @staticmethod
    def vertical_gradient(rgba_top, rgba_bottom):
        texture = Texture.create(size=(1, 2), colorfmt="rgba")
        pixels = rgba_bottom + rgba_top
        pixels = [chr(int(v * 255)) for v in pixels]
        buf = ''.join(pixels)
        texture.blit_buffer(buf, colorfmt='rgba', bufferfmt='ubyte')
        return texture
