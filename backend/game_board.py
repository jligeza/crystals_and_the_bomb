#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen
from kivy.properties import NumericProperty, ObjectProperty
from kivy.clock import mainthread
from kivy.uix.modalview import ModalView
from bot import Bot
from kivy.clock import Clock


class GuiBarrier(ModalView):

    opacity = 0

    def activate(self, parent_screen):
        parent_screen.add_widget(self)

    def deactivate(self, parent_screen):
        parent_screen.remove_widget(self)


class GameBoard(Screen):

    player_1 = ObjectProperty()
    player_2 = ObjectProperty()
    shared_area = ObjectProperty()
    turn_bar = ObjectProperty()
    gui_barrier = GuiBarrier()

    MAX_PICKS_PER_TURN = 3
    winner = None
    loser = None
    _player_turn = NumericProperty()
    _current_player = ObjectProperty()
    _game_mode = None
    _bot = None
    difficulty = None
    gui_frozen = None

    @property
    def player_turn(self):
        return self._player_turn

    @player_turn.setter
    def player_turn(self, value):
        raise ValueError('read only - use switch_turn')

    @property
    def current_player(self):
        return self._current_player

    @current_player.setter
    def current_player(self, value):
        raise ValueError('read only')

    @property
    def game_mode(self):
        '''Single or multi player.'''
        return self._game_mode

    @game_mode.setter
    def game_mode(self, value):
        if 'single' in value:
            self._game_mode = 'single'
        elif 'multi' in value:
            self._game_mode = 'multi'
        else:
            raise ValueError('allowed only single or multi')

    def __init__(self, **kwargs):
        super(GameBoard, self).__init__(**kwargs)
        self._register_events()
        self._configure_after_kv_load()

    def _register_events(self):
        self.register_event_type('on_match_finish')
        self.register_event_type('on_match_start')

    def on_match_finish(self, winner, score):
        pass

    def on_match_start(self):
        pass

    def on_pre_enter(self):
        self._disable_pass_button()
        if self.game_mode == 'single':
            self.player_1.player_name = 'Player'
            self.player_2.player_name = 'your phone'
            self._bot.set_difficulty(self.difficulty)

        Clock.schedule_once(lambda x: self._start_game(), 1)

    def _start_game(self):
        self.dispatch('on_match_start')

        if self.game_mode == 'single':
            message = 'your turn'
        else:
            message = 'Player 1 begins'

        self.turn_bar.show(text=message)
        self._bot.enable()

    @mainthread
    def _configure_after_kv_load(self):
        self._bind_events()
        self._player_turn = 1
        self._current_player = self.player_1
        self._bot = Bot(
            player=self.player_2,
            shared_area=self.shared_area
        )

    def _bind_events(self):
        self.shared_area.bind(on_pass=self._on_pass)

        self.player_1.bind(on_pass=self._on_pass)
        self.player_2.bind(on_pass=self._on_pass)

        self.player_1.bind(on_timeout=self._on_pass)
        self.player_2.bind(on_timeout=self._on_pass)

        self.shared_area.bind(on_item_score=self._on_item_score)

        self.turn_bar.bind(on_bar_show=self._on_bar_show)
        self.turn_bar.bind(on_bar_hide=self._on_bar_hide)

    def _on_pass(self, obj):
        if self._player_did_not_pick_this_turn():
            self._force_pick(self.current_player)
        self.shared_area.this_turn_picks = 0
        self.switch_turn()

    def _player_did_not_pick_this_turn(self):
        return self.shared_area.this_turn_picks == 0

    def _force_pick(self, player):
        self.shared_area.claim_random_item(player)

    def on__player_turn(self, *x):
        self._show_turn_bar()

        if self.current_player == self.player_1:
            self._current_player = self.player_2
        else:
            self._current_player = self.player_1

        if self._bot_turn():
            self._bot.make_move()

    def _show_turn_bar(self):
        message = self._prepare_message_for_turn_bar()
        self.turn_bar.show(message)

    def _prepare_message_for_turn_bar(self):
        try:
            if self.current_player == self.player_1:
                return self.player_2.player_name + ' turn'
            else:
                if 'single' in self.game_mode:
                    return 'your turn'
                else:
                    return self.player_1.player_name + ' turn'
        except (AttributeError, TypeError):
            return 'pass'

    def _bot_turn(self):
        return self.game_mode == 'single' and\
            self.player_2 == self.current_player

    def _disable_pass_button(self):
        self.shared_area.pass_button.disabled = True

    def _enable_pass_button(self):
        self.shared_area.pass_button.disabled = False

    def _on_item_score(self, shared_area, player):
        if shared_area.this_turn_picks >= self.MAX_PICKS_PER_TURN:
            shared_area.end_turn()
            shared_area.this_turn_picks = 0

    def _on_bar_show(self, *x):
        self.freeze_gui()
        self.player_1.stop_timer()
        self.player_2.stop_timer()

    def _on_bar_hide(self, *x):
        if not self._bot_turn():
            self.activate_gui()
        self.current_player.start_timer()

    def freeze_gui(self):
        if not self.gui_frozen:
            self.gui_frozen = True
            self.gui_barrier.activate(self)
            self._disable_pass_button()

    def activate_gui(self):
        if self.gui_frozen is True:
            self.gui_frozen = False
            self.gui_barrier.deactivate(self)
            self._enable_pass_button()

    def switch_turn(self):
        if self._player_turn == 1:
            self._player_turn = 2
        else:
            self._player_turn = 1

    def finish_match(self, player):
        self._bot.disable()
        self._set_winner_and_loser(player)
        self._disable_pass_button()
        self._reset_score_of_loser()

        self.dispatch(
            'on_match_finish',
            winner=self.winner.player_name,
            score=self.winner.score
        )

    def _set_winner_and_loser(self, player_name):
        if player_name == self.player_1.player_name:
            self.loser = self.player_1
            self.winner = self.player_2
        elif player_name == self.player_2.player_name:
            self.loser = self.player_2
            self.winner = self.player_1
        else:
            raise Exception('no such player')

    def _reset_score_of_loser(self):
        self.loser.score = 0

    def reset(self):
        self.winner = None
        self.loser = None

        self._player_turn = 1

        self.player_1.reset(player_name='Player 1')
        self.player_2.reset(player_name='Player 2')
        self.shared_area.reset()
