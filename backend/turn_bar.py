from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.animation import Animation


Builder.load_string('''
#:import Window kivy.core.window.Window
<TurnBar>:
    size_hint: None, None
    size: Window.width, '150sp'
    text: 'turn test'
    font_size: '40sp'
    pos_hint: {'center_x': 0.5, 'center_y': 0.5}
    opacity: 0

    canvas.before:
        Color:
            rgb: 0.1, 0.1, 0.3
        Rectangle:
            pos: self.pos
            size: self.size
''')


class TurnBar(Label):

    full_opacity = 0.8
    show_hide_duration = 0.3

    def __init__(self, **kwargs):
        super(TurnBar, self).__init__(**kwargs)
        self.register_event_type('on_bar_show')
        self.register_event_type('on_bar_hide')

    def on_bar_show(self):
        pass

    def on_bar_hide(self):
        pass

    def show(self, text='pass', seconds=1):
        self.dispatch('on_bar_show')

        self.color = (.9, .9, .9, 1)
        self.text = text

        anim = Animation(
            opacity=self.full_opacity,
            duration=self.show_hide_duration
        )
        anim.bind(
            on_complete=lambda a, w: self._anim_show_bar(seconds)
        )
        anim.start(self)

    def _anim_show_bar(self, seconds):
        anim = Animation(
            opacity=self.full_opacity,
            duration=seconds
        )
        anim.bind(on_complete=self._anim_keep_bar)
        anim.start(self)

    def _anim_keep_bar(self, *x):
        anim = Animation(
            opacity=0,
            duration=self.show_hide_duration
        )
        anim.bind(on_complete=self._anim_hide_bar)
        anim.start(self)

    def _anim_hide_bar(self, *x):
        self.dispatch('on_bar_hide')
