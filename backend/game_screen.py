#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty
from kivy.clock import mainthread


class GameScreen(Screen):

    game_board = ObjectProperty()
    options_screen = ObjectProperty()
    inner_screen_manager = ObjectProperty()

    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)
        self._register_events()
        self._bind_events()

    def _register_events(self):
        self.register_event_type('on_match_finish')

    def on_match_finish(self, winner, score):
        pass

    @mainthread
    def _bind_events(self):
        self.game_board.bind(
            on_match_finish=self._on_match_finish
        )
        self.options_screen.bind(
            on_single_player_chosen=self._on_single_player_chosen
        )
        self.options_screen.bind(
            on_multiplayer_chosen=self._on_multiplayer_chosen
        )

    def _on_match_finish(self, game_board, winner, score):
        self.dispatch('on_match_finish', winner, score)

    def _on_single_player_chosen(self, obj, difficulty):
        self._start_game(game_mode='single_player', difficulty=difficulty)

    def _start_game(self, game_mode, difficulty=None):
        self.game_board.game_mode = game_mode
        self.game_board.difficulty = difficulty
        self.inner_screen_manager.current = 'game_board'

    def _on_multiplayer_chosen(self, obj):
        self._start_game(game_mode='multiplayer')

    def on_leave(self):
        self.inner_screen_manager.current = 'options_screen'

    def reset(self):
        self.game_board.reset()

    def retry(self):
        self.game_board.reset()
        self.inner_screen_manager.current = 'game_board'
