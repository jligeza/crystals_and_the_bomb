#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.gridlayout import GridLayout
from kivy.clock import mainthread, Clock
from kivy.properties import ObjectProperty, NumericProperty
import random


class SharedArea(GridLayout):
    '''
    Part of game screen, where crystals and a bomb are displayed.
    '''

    game_board = ObjectProperty()
    shared_pool = ObjectProperty()
    pass_button = ObjectProperty()

    num_unclaimed_crystals = NumericProperty()
    defeat_delay = 0.5
    this_turn_picks = 0
    _scored_items = []

    BOMB_ID = 0

    def __init__(self, **kwargs):
        super(SharedArea, self).__init__(**kwargs)
        self.register_event_type('on_item_score')
        self.register_event_type('on_pass')
        self._configure_after_kv_load()

    def on_item_score(self, player):
        pass

    def on_pass(self):
        pass

    @mainthread
    def _configure_after_kv_load(self):
        self._bind_events()
        self.num_unclaimed_crystals =\
            len(self.shared_pool.children) - 1

    def _bind_events(self):
        self.shared_pool.bind(
            on_item_collect=lambda x, item: self._score_item(item)
        )

    def _score_item(self, item, player=None):
        if not player:
            player = self.game_board.current_player

        self._scored_items.append(item)

        self._move_item_to_players_area(item, player)

        if self._picked_crystal(item):
            self._score_crystal(player)
        elif self._picked_bomb(item):
            self._score_bomb(player)

        self.this_turn_picks += 1
        self.dispatch('on_item_score', player)

    def _move_item_to_players_area(self, item, player):
        container = player.ids.points_container
        pos = container.get_free_pos()
        container.add(item)
        item.move(pos)

    def _picked_crystal(self, item):
        if item.type == 'crystal':
            return True
        return False

    def _score_crystal(self, player):
        self.shared_pool.crystals_count -= 1
        player.score += 1

    def _picked_bomb(self, item):
        if item.type == 'bomb':
            return True
        return False

    def _score_bomb(self, player):
        self._lock_all_items()
        player_name = player.player_name
        Clock.schedule_once(
            lambda dt: self.game_board.finish_match(player_name),
            self.defeat_delay
        )

    def _lock_all_items(self):
        for item in self.shared_pool.children:
            item.disabled = True

    def claim_random_item(self, player):
        if self._is_anything_to_claim():
            item = self._select_random_item_from_shared_pool()
            self._score_item(item, player)

    def _is_anything_to_claim(self):
        return len(self._scored_items) <\
            self.shared_pool.INITIAL_NUMBER_OF_CRYSTALS + 1

    def _select_random_item_from_shared_pool(self):
        if self._all_crystals_already_claimed():
            bomb = self.shared_pool.children[self.BOMB_ID]
            return bomb
        else:
            return self._random_crystal()

    def _random_crystal(self):
        while True:
            item = random.choice(self._all_crystals())
            if self._is_unclaimed(item):
                return item

    def _all_crystals(self):
        return self.shared_pool.children[self.BOMB_ID + 1:]

    def _all_crystals_already_claimed(self):
        return len(self._scored_items) ==\
            self.shared_pool.INITIAL_NUMBER_OF_CRYSTALS

    def _is_unclaimed(self, item):
        return item not in self._scored_items

    def end_turn(self):
        self.dispatch('on_pass')

    def reset(self):
        self.shared_pool.reset()
        self.this_turn_picks = 0
        self._scored_items = []
