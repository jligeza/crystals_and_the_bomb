from kivy.uix.screenmanager import Screen


class OptionsScreen(Screen):

    def __init__(self, **kwargs):
        super(OptionsScreen, self).__init__(**kwargs)
        self._register_events()

    def _register_events(self):
        self.register_event_type('on_single_player_chosen')
        self.register_event_type('on_multiplayer_chosen')

    def on_single_player_chosen(self, difficulty):
        pass

    def on_multiplayer_chosen(self):
        pass

    def choose_single_player(self, difficulty):
        self.dispatch('on_single_player_chosen', difficulty)

    def choose_multiplayer(self):
        self.dispatch('on_multiplayer_chosen')
