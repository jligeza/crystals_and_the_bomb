#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.relativelayout import FloatLayout
from kivy.metrics import sp


class PlayerPool(FloatLayout):

    ITEM_WIDTH = sp(20)

    def __init__(self, **kwargs):
        super(PlayerPool, self).__init__(**kwargs)
        self.items = []
        self.number_of_items = 0

    def add(self, crystal):
        self.number_of_items += 1
        self.items.append(crystal)

    def get_free_pos(self):
        if len(self.items) == 0:
            return self.pos
        else:
            return self._next_pos()

    def _next_pos(self):
        x = self.number_of_items * self.ITEM_WIDTH + self.pos[0]
        y = self.pos[1]
        return [x, y]

    def reset(self):
        self.items = []
        self.number_of_items = 0
