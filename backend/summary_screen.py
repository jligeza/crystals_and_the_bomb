#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import Screen


class SummaryScreen(Screen):

    def __init__(self, **kwargs):
        super(SummaryScreen, self).__init__(**kwargs)
        self.register_event_type('on_retry')

    def on_retry(self):
        pass

    def retry(self):
        self.dispatch('on_retry')
