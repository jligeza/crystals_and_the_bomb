#!/usr/bin/env python
# -*- coding: utf-8 -*-
from backend.collectible_item import Crystal, Bomb
from kivy.metrics import sp
from kivy.uix.relativelayout import FloatLayout


class SharedPool(FloatLayout):
    '''
    Draws a screen-wide pool with clickable items.

    gap_column: a number of gap columns between the
        pool and right screen border.
    '''

    INITIAL_NUMBER_OF_CRYSTALS = 20
    item_size = sp(60)
    gap_column = 1

    def __init__(self, **kwargs):
        super(SharedPool, self).__init__(**kwargs)
        self._rows = 1
        self._row_len = 0
        self._initialized = False
        self.register_event_type('on_item_collect')

    def on_height(self, *x):
        '''Initialize when UI is ready.'''
        if self.height > 100 and not self._initialized:
            self._initialize()
            self._initialized = True

    def _initialize(self):
        for i in xrange(self.INITIAL_NUMBER_OF_CRYSTALS):
            self._add_crystals(i)
        self._add_bomb()

    def _add_crystals(self, i):
        crystal = Crystal(pos=self.get_free_pos(), id=str(i))
        crystal.bind(on_press=self._item_touched)
        self.add_widget(crystal)

    def _add_bomb(self):
        bomb_id = str(self.INITIAL_NUMBER_OF_CRYSTALS)
        bomb = Bomb(pos=self.get_free_pos(), id=bomb_id)
        bomb.bind(on_press=self._item_touched)
        self.add_widget(bomb)

    def get_free_pos(self):
        if len(self.children) == 0:
            return self._get_initial_pos()
        else:
            if not self._too_many_items_in_row():
                return self._get_pos_for_current_row()
            else:
                return self._get_pos_for_next_row()

    def _get_initial_pos(self):
        x = self.pos[0]
        y = self.pos[1] + self.height - self.item_size
        self._row_len += self.item_size
        return [x, y]

    def _too_many_items_in_row(self):
        return self._row_len +\
            (self.item_size * self.gap_column + 1) > self.width

    def _get_pos_for_current_row(self):
        prev = self.children[0]
        x = prev.pos[0] + self.item_size
        y = prev.pos[1]
        self._row_len += self.item_size
        return [x, y]

    def _get_pos_for_next_row(self):
        first = self.children[-1]

        x = first.pos[0]
        y = first.pos[1] - self._rows * self.item_size

        self._rows += 1
        self._row_len = self.item_size
        return [x, y]

    def _item_touched(self, item):
        self.dispatch('on_item_collect', item)

    def on_item_collect(self, item):
        pass

    def reset(self):
        self.clear_widgets()
        self._rows = 1
        self._row_len = 0
        self._initialize()
        self._this_turn_picks = 0
