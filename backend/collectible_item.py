#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.animation import Animation
from kivy.uix.image import Image
from kivy.uix.behaviors.button import ButtonBehavior
from kivy.lang import Builder

Builder.load_string('''
<CollectibleItem>
    size_hint: None, None
    size: '60sp', '60sp'

<Crystal>:
    source: './images/crystal.png'

<Bomb>:
    source: './images/bomb.png'
''')


class CollectibleItem(ButtonBehavior, Image):

    def on_press(self):
        self.disabled = True

    def move(self, pos):
        anim = Animation(
            pos=pos,
            size=self._half_size(),
            duration=0.5,
            transition='out_bounce'
        )
        anim.bind(on_complete=self._on_anim_complete)
        anim.start(self)

    def _half_size(self):
        return [self.size[0] / 2, self.size[1] / 2]

    def _on_anim_complete(self, animation, widget):
        pass


class Crystal(CollectibleItem):

    type = 'crystal'


class Bomb(CollectibleItem):

    type = 'bomb'
