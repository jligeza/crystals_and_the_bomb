from kivy.uix.widget import Widget
from kivy.clock import Clock


class Bot(Widget):

    player = None
    shared_area = None
    action_delay = None
    difficulty = None
    _borders = None

    def __init__(self, player, shared_area, action_delay=2, **kwargs):
        super(Bot, self).__init__(**kwargs)

        self.player = player
        self.action_delay = action_delay
        self.shared_area = shared_area

    def make_move(self):
        Clock.schedule_once(
            lambda dt: self.score_items(),
            self.action_delay
        )

    def score_items(self):
        if self.ready:
            for i in xrange(3):
                if self._safe_pick():
                    self.shared_area.claim_random_item(self.player)
                else:
                    return self.shared_area.end_turn()

    def _safe_pick(self):
        return self.shared_area.shared_pool.crystals_count\
            not in self._borders

    def disable(self):
        self.ready = False

    def enable(self):
        self.ready = True

    def set_difficulty(self, difficulty):
        if difficulty not in ['easy', 'medium', 'hard']:
            difficulty = 'easy'

        if difficulty == 'easy':
            self._borders = [0, 4]
        elif difficulty == 'medium':
            self._borders = [0, 4, 8]
        elif difficulty == 'hard':
            self._borders = [0, 4, 8, 12]
