[app]

title = Crystals and the Bomb 
package.name = crystals_and_the_bomb 
package.domain = org.jligeza 
source.dir = .  
source.include_exts = py,png,jpg,kv,gif,rst,ttf
version = 1.0.7
requirements = kivy==master,docutils
orientation = portrait
fullscreen = 1 
android.api = 19 
android.minapi = 13 
presplash.filename = images/splash_screen.jpg
icon.filename = images/crystal.png

[buildozer] 

log_level = 1 
warn_on_root = 1
