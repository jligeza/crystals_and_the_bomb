#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.core.window import Window


class Game(App):
    '''
    Manages game states.
    '''

    def on_pause(self):
        return True

    def on_start(self, *x):
        self.game_screen = self.root.ids.game_screen
        self.summary_screen = self.root.ids.summary_screen
        # Window.clearcolor = (.9, .9, .9, 1)
        self._bind_events()

    def _bind_events(self):
        self.game_screen.bind(
            on_match_finish=self._match_finished
        )
        self.summary_screen.bind(on_retry=self._on_retry)

    def _match_finished(self, obj, winner, score):
        self._swap_to_summary_screen(winner, score)
        self.game_screen.reset()

    def _swap_to_summary_screen(self, winner, score):
        self.summary_screen.winner_name = winner
        self.summary_screen.score = score
        self.root.current = 'summary'

    def _on_retry(self, *x):
        self.game_screen.retry()
        self.root.current = 'game'

    def reset_game_state(self):
        self.game_screen.reset()

Game().run()
