How to win
==========
A player wins after forcing their opponent to pick a bomb.

Every turn, an active player has 10 seconds to claim 1, 2 or 3 items - crystals or a bomb.

If the time runs out, and no item was claimed, one random item is selected for the player. A bomb
cannot be randomly picked, if there is at least one free crystal on the board.

Single player
=============
Play against a bot, which has available three levels of difficulty.

Multiplayer
===========
Play with your friend on a single device.
